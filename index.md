---
layout: default
permalink: /

---



Bienvenue sur Linking the Past, un projet d'annexes interactives associées à ma [thèse de doctorat](/about/).

Ce site est pour l'instant seulement disponible en français, merci de votre compréhension !

## - 1/ [Configuration](/config) de la Wikibase

## - 2/ [Interrogation](/query) de la Wikibase

## - 3/ [Requêtes fédérées](/feder) (entre la Wikibase et Wikidata)

## - 4/ [Réconciliation de données](/recon) à partir de la Wikibase