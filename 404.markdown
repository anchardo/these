---
layout: default
title: "404: Page not found"
permalink: /404.html
---

<div class="page">
  <h1 class="page-title">404 : page non trouvée</h1>
  <p class="lead">Oups, l'URL demandée n'a pas été trouvée :(<br>
  	<a href="https://linkingthepast.org">Retour à la page d'accueil</a>, ou <a href="about">sur la page de contact</a>.</p>
</div>
