---
layout: page
title: À propos
permalink: /about/
---

### Contexte
_Linking the Past_ est une initiative de **Anne Chardonnens** (Docteure en Information et Communication, actuellement Responsable de l'Accès numérique aux Collections au [CegeSoma](https://www.cegesoma.be/fr/anne-chardonnens)/Archives de l'État. Ce site est né du besoin de pouvoir partager de façon interactive des exemples dans le cadre de [ma thèse de doctorat](http://hdl.handle.net/2013/ULB-DIPOT:oai:dipot.ulb.ac.be:2013/315804), réalisée à l'[Université libre de Bruxelles](https://www.ulb.be/) sous la direction de [Seth van Hooland](https://twitter.com/sethvanhooland/).

### Objectif
Ce site est destiné à accueillir des exemples illustrant les différentes étapes prenant place dans le cadre de la [publication de données d'autorité](https://www.wikidata.org/wiki/Wikidata:Events/LkPast-WB) sur une instance [Wikibase](https://wikiba.se). Il permet de combiner textes, images, liens ou encore [Jupyter notebooks](https://jupyter.org/).

### Ce site
Ce site est publié à l'aide de [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) et du générateur de sites statiques [Jekill](http://jekyllrb.com/). 

Il est principalement inspiré du [carnet de Antoine Fauchié](https://www.quaternum.net/) et du site dédié au projet [Legacies of Catalogues and Curatorial Voice](https://cataloguelegacies.github.io/), ainsi que de [cet exemple](http://www.kasimte.com/adding-and-including-jupyter-notebooks-as-jekyll-blog-posts) pour ce qui est de l'intégration de _Jupyter Notebooks_.

### Contact
Je suis sur Twitter : [@annechardo](https://www.twitter.com/annechardo).
